﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using Nytrogen.Lib;

namespace WpfApp1
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            foreach (TextBox tx in GridData.Children.OfType<TextBox>())
            {
                if (tx.Text.ToString() == string.Empty)
                {
                    MessageBox.Show("Необходимо заполнить все данные");
                    return;
                }

            }
            try
               {
                   Formula.B = Convert.ToDouble(textBoxB.Text);
                   Formula.M = Convert.ToDouble(textBoxM.Text);
                   Formula.GM = Convert.ToDouble(textBoxGM.Text);
                   Formula.QNRM = Convert.ToDouble(textBoxQNRM.Text);
                   Formula.QNRG = Convert.ToDouble(textBoxQNRG.Text);
                   Formula.ALFA = Convert.ToDouble(textBoxALFA.Text);
                   Formula.KP = Convert.ToDouble(textBoxKP.Text);
                   Formula.VK = Convert.ToDouble(textBoxVK.Text);
                   Formula.VKB = Convert.ToDouble(textBoxVKB.Text);
                   Formula.VBB = Convert.ToDouble(textBoxVBB.Text);
                   Formula.NR = Convert.ToDouble(textBoxNR.Text);
                   Formula.VKV = Convert.ToDouble(textBoxVKV.Text);
                   Formula.N2K = Convert.ToDouble(textBoxN2K.Text);
                   Formula.FG = Convert.ToDouble(textBoxFG.Text);
                
               }
               catch (FormatException)
               {
                MessageBox.Show("Неверно введено число. \nВнимательно проверьте введённые значения");
               } 

               if (Formula.B == 0 || Formula.M == 0 || Formula.GM == 0 ||
                    Formula.QNRM == 0 || Formula.QNRG == 0 || Formula.ALFA == 0
                   || Formula.KP == 0 || Formula.VK == 0 || Formula.VKB == 0 ||
                    Formula.VBB == 0 || Formula.NR == 0 || Formula.N2K == 0 || Formula.FG == 0)
               {
                   MessageBox.Show("Это значение не может равняться нулю");
               }
               else
               {
                   Formula.O2D = Formula.O2(Formula.VK, Formula.VKB, Formula.VBB);
                   Formula.VALFAD = Formula.VALFA(Formula.B, Formula.M, Formula.GM, Formula.QNRM, Formula.QNRG, Formula.ALFA, Formula.O2D);
                   Formula.TBD = Formula.TB(Formula.GM, Formula.ALFA);
                   Formula.TD = Formula.T(Formula.KP, Formula.TBD);
                   Formula.O2ID = Formula.O2I(Formula.GM, Formula.ALFA, Formula.KP, Formula.TBD);
                   Formula.N2D = Formula.N2(Formula.O2D, Formula.ALFA, Formula.GM);
                   Formula.NORD = Formula.NOR(Formula.KP, Formula.N2D, Formula.O2ID, Formula.TBD);
                   Formula.NO1D = Formula.NO1(Formula.KP, Formula.NORD, Formula.TBD, Formula.TD);
                   Formula.VNOD = Formula.VNO(Formula.B, Formula.M, Formula.GM, Formula.QNRM, Formula.NR, Formula.VKV, Formula.N2K, Formula.NO1D, Formula.VALFAD);
                   Formula.MCNOD = Formula.MCNO(Formula.B, Formula.M, Formula.GM, Formula.QNRM, Formula.NR, Formula.VKV, Formula.N2K, Formula.NO1D, Formula.VALFAD);
                   Formula.MYNOD = Formula.MYNO(Formula.B, Formula.M, Formula.GM, Formula.QNRM, Formula.NR, Formula.VKV, Formula.N2K, Formula.NO1D, Formula.VALFAD);
                   Formula.MNOD = Formula.MNO(Formula.B, Formula.M, Formula.GM, Formula.QNRM, Formula.NR, Formula.VKV, Formula.N2K, Formula.NO1D, Formula.VALFAD);
                   Formula.NOD = Formula.NO(Formula.VNOD, Formula.VALFAD);

                   textBoxTB.Text = Convert.ToString(Formula.TBD);
                   textBoxT.Text = Convert.ToString(Formula.TD);
                   textBoxVALFA.Text = Convert.ToString(Formula.VALFAD);
                   textBoxO2I.Text = Convert.ToString(Formula.O2ID);
                   textBoxN2.Text = Convert.ToString(Formula.N2D);
                   textBoxNO1.Text = Convert.ToString(Formula.NO1D);
                   textBoxNOR.Text = Convert.ToString(Formula.NORD);
                   textBoxMCNO.Text = Convert.ToString(Formula.MCNOD);
                   textBoxMYNO.Text = Convert.ToString(Formula.MYNOD);
                   textBoxMNO.Text = Convert.ToString(Formula.MNOD);
                   textBoxVNO.Text = Convert.ToString (Formula.VNOD);
                   textBoxNO.Text = Convert.ToString(Formula.NOD);
                   tabControl.SelectedIndex = 2;
               } 
        }
        private void Button_Discharge(object sender, RoutedEventArgs e)
        {
            foreach (TextBox tx in GridData.Children.OfType<TextBox>())
            {
                if (tx is TextBox)
                    (tx).Text = null;

            }
            foreach (TextBox tx in GridResults.Children.OfType<TextBox>())
            {
                if (tx is TextBox)
                    (tx).Text = null;

            }
          

        }
        private void textBoxB_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789,".IndexOf(e.Text) < 0;
        }

        private void textBoxM_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789,".IndexOf(e.Text) < 0;
        }

        private void textBoxGM_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789,".IndexOf(e.Text) < 0;
        }

        private void textBoxQNRM_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789,".IndexOf(e.Text) < 0;
        }

        private void textBoxQNRG_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789,".IndexOf(e.Text) < 0;
        }

        private void textBoxALFA_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789,".IndexOf(e.Text) < 0;
        }

        private void textBoxKP_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789,".IndexOf(e.Text) < 0;
        }

        private void textBoxVK_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789,".IndexOf(e.Text) < 0;
        }

        private void textBoxVKB_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789,".IndexOf(e.Text) < 0;
        }

        private void textBoxVBB_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789,".IndexOf(e.Text) < 0;
        }

        private void textBoxNR_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789,".IndexOf(e.Text) < 0;
        }

        private void textBoxVKV_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789,".IndexOf(e.Text) < 0;
        }

        private void textBoxN2K_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789,".IndexOf(e.Text) < 0;
        }

        private void textBoxFG_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789,".IndexOf(e.Text) < 0;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var form = new WindowReport();
            form.ShowDialog();
        }

        private void Button_Default(object sender, RoutedEventArgs e)
        {
            textBoxB.Text = "133,7";
            textBoxM.Text = "42,85";
            textBoxGM.Text = "0,205";
            textBoxQNRM.Text = "40190";
            textBoxQNRG.Text = "33410";
            textBoxALFA.Text = "1,345";
            textBoxKP.Text = "0,81";
            textBoxVK.Text = "1500";
            textBoxVKB.Text = "3500";
            textBoxVBB.Text = "47187";
            textBoxNR.Text = "0,5";
            textBoxVKV.Text = "0";
            textBoxN2K.Text = "2,85";
            textBoxFG.Text = "107,4";

            
        
        }
            private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Process proc = new Process();
            proc.StartInfo = new ProcessStartInfo()
            {
                FileName = "Manual.pdf"
            };
            proc.Start();
        }

        private void textBoxM_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

  

        private void tabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        
    }
}
