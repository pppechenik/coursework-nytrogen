﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Nytrogen.Lib;

namespace WpfApp1
{
    /// <summary>
    /// Логика взаимодействия для WindowReport.xaml
    /// </summary>
    public partial class WindowReport : Window
    {
        public WindowReport()
        {
            InitializeComponent();
            var r = new ReportViewer
            {
                ProcessingMode = ProcessingMode.Local
            };
            r.LocalReport.ReportPath = "ReportAzot.rdlc";
            WinFormHost.Child = r;
            var ds = new DataSet1();
            var t = ds.Tables["DataReport"];
            AddRow(t, "Балансовая температура", Formula.TBD, "К");
            AddRow(t, "Температура газовой среды", Formula.TD, "К");
            AddRow(t, "Концентрация избыточного кислорода в продуктах сгорания", Formula.O2ID, "%");
            AddRow(t, "Концентрация азота в продуктах сгорания", Formula.N2D, "%");
            AddRow(t, "Равновесная концентрация оксида азота", Formula.NORD, "%");
            AddRow(t, "Массовая концентрация оксида азота", Formula.MCNOD, "кг/м\u00B3");
            AddRow(t, "Скорость образования продуктов сгорания", Formula.VALFAD, "м\u00B3/с");
            AddRow(t, "Равновесная концентрация оксида азота с учетом пульсации температуры", Formula.NO1D, "%");
            AddRow(t, "Удельное количество оксида азота", Formula.MYNOD, "кг/т (стали)");
            AddRow(t, "Массовая скорость образования оксида азота", Formula.MNOD, "кг/ч");
            AddRow(t, "Объемная скорость образования оксида азота", Formula.VNOD, "м\u00B3/ч");
            AddRow(t, "Концентрация оксида азота в продуктах сгорания", Formula.NOD, "% (об.)");
            r.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", t));
            r.RefreshReport();
            
        }

        private void AddRow(System.Data.DataTable t, string name, object value, string razm)
        {
            var row = t.NewRow();
            row["Name"] = name;
            row["Value"] = value;
            row["Razm"] = razm;
            t.Rows.Add(row);
        }

    }
} 
