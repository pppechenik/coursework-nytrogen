﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nytrogen.Lib
{
    public static class Formula
    {
        public static double B { get; set; }
        public static double M { get; set; }
        public static double GM { get; set; }
        public static double QNRM { get; set; }
        public static double QNRG { get; set; }
        public static double ALFA { get; set; }
        public static double KP { get; set; }
        public static double VK { get; set; }
        public static double VKB { get; set; }
        public static double VBB { get; set; }
        public static double NR { get; set; }
        public static double VKV { get; set; }
        public static double N2K { get; set; }
        public static double FG { get; set; }
        public static double O2D { get; set; }
        public static double VALFAD { get; set; }
        public static double TBD { get; set; }
        public static double TD { get; set; }
        public static double O2ID { get; set; }
        public static double N2D { get; set; }
        public static double NORD { get; set; }
        public static double NO1D { get; set; }
        public static double VNOD { get; set; }
        public static double MCNOD { get; set; }
        public static double MYNOD { get; set; }
        public static double MNOD { get; set; }
        public static double NOD { get; set; }

        
        public static double TB (double gm, double alfa)
        {
            return Math.Round(2440 / Math.Pow(alfa, 0.27) * Math.Exp((0.135 - 0.03 * alfa) * gm));
        }

    
        public static double T (double kp, double tb)
        {
            return Math.Round(tb - 2*(tb-(tb*kp))/3);
        }

     
        public static double O2I(double gm, double alfa, double kp, double tb)
        {
            return Math.Round(Math.Log(alfa) * (((1.84 - alfa) * gm) + (23.69 - 5.3 * alfa)) + ((6.078 * gm * Math.Pow(1.0048,(tb * kp - 273)) + (65.8 * Math.Pow(1.0047,(tb * kp - 273)))) * Math.Pow(10,-6)),2);
        }

     
        public static double O2(double vk, double vkb, double vbb)
        {
            return Math.Round((vk + 0.2568 * (vbb + vkb)) / (vbb + vkb + vk) * 100,2);
        }

        public static double VALFA(double b, double m, double gm, double qnrm, double qnrg, double alfa, double o2)
        {
            return Math.Round((b * m *  ((217.47 * alfa - 52.3) * Math.Pow(o2, (-0.88 * Math.Pow(alfa, 0.11))) * 29310)) / (3600 * qnrm) + ((b * m * (1 - gm) * (182 * alfa - 56) * Math.Pow(o2, (-0.82 * Math.Pow(alfa, 0.16))) / (3600 * qnrg))),3);
        }

       
        public static double N2(double o2, double alfa, double gm)
        {
            return Math.Round(84.943 - (0.9159 * o2) + (3.2501 * alfa) + (2.0977 * gm),2);
        }

     
        public static double NOR(double kp, double n2, double o2i, double tb)
        {
            return Math.Round(Math.Sqrt(n2 * o2i) * Math.Exp(-21500 / (tb * kp)),5);
        }

        public static double NO1(double kp, double nor, double tb, double t)
        {
            return Math.Round(nor * (1 + Math.Pow(Math.Sqrt(Math.Pow(t-(tb*kp),2))/(tb*kp),2)/2 * (0.5*(Math.Pow(Math.Exp(-67500/t),2) - Math.Pow(Math.Exp(-46000/t),2)) + (Math.Exp(-67500/t) - Math.Exp(-46000/t)))),5);
        }

      
        public static double MCNO(double b, double m, double gm, double qnrm, double nr, double vkv, double n2k, double no1, double valfa)
        {
            return Math.Round((0.01 * no1 * 1.3402) + (2.1422 * (0.01 * nr * b * m * gm / (qnrm / 29310)) / (3600 * valfa)) + (0.021422 * vkv * n2k * 1.2505 / (3600 * valfa)),6);
        }


        public static double MYNO(double b, double m, double gm, double qnrm, double nr, double vkv, double n2k, double no1, double valfa)
        {
            return Math.Round((0.01 * valfa * no1 * 1.3402 / m) + (2.1422 * 0.01 * nr * b * m * gm / (qnrm / 29310) / m) + ((0.021422 * vkv * n2k * 1.2505) / m),3);
        }

      
        public static double MNO(double b, double m, double gm, double qnrm, double nr, double vkv, double n2k, double no1, double valfa)
        {
            return Math.Round((0.01*valfa*no1*1.3402) + (2.1422*0.01*nr*b*m*gm/(qnrm/29310)) + (0.021422*vkv*n2k*1.2505),3);
        }

    
        public static double VNO(double b, double m, double gm, double qnrm, double nr, double vkv, double n2k, double no1, double valfa)
        {
            return Math.Round(((0.01 * valfa * no1) + ((2.1422 * 0.01 * nr * b * m * gm / (qnrm / 29310) / 1.3402) + (0.02142 * vkv * n2k))),3);
        }

        
        public static double NO(double vno, double valfa)
        {
            return Math.Round(vno / (3600 * valfa)*100,3);
        }
    }
}
