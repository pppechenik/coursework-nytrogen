﻿namespace ReportsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components;

        /// <summary>
        /// Очистка всех занятых ресурсов.
        /// </summary>
        /// <param name="disposing">Значение true, если следует освободить управляемые ресурсы, в противном случае значение false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, формируемый конструктором Windows Form Designer 

        /// <summary>
        /// Обязательный метод для поддержки конструктора Designer, не изменяйте
        /// его содержимое в редакторе кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.MainWindowBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.taskbarItemInfoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.allowsTransparencyDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.titleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iconDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sizeToContentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.topDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.leftDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.restoreBoundsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.windowStartupLocationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.showInTaskbarDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.isActiveDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ownerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ownedWindowsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dialogResultDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.windowStyleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.windowStateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.resizeModeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.topmostDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.showActivatedDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.contentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contentTemplateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contentTemplateSelectorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contentStringFormatDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.borderBrushDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.borderThicknessDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.backgroundDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.foregroundDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fontFamilyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fontSizeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fontStretchDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fontStyleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fontWeightDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.horizontalContentAlignmentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.verticalContentAlignmentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabIndexDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isTabStopDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.paddingDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.templateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.styleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.overridesDefaultStyleDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.useLayoutRoundingDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.templatedParentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.resourcesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataContextDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindingGroupDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.languageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tagDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inputScopeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.actualWidthDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.actualHeightDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.layoutTransformDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.widthDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.minWidthDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maxWidthDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.heightDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.minHeightDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maxHeightDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowDirectionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.marginDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.horizontalAlignmentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.verticalAlignmentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.focusVisualStyleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cursorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forceCursorDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.isInitializedDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.isLoadedDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.toolTipDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.parentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hasAnimatedPropertiesDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.allowDropDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.desiredSizeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isMeasureValidDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.isArrangeValidDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.renderSizeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.renderTransformDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.renderTransformOriginDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isMouseDirectlyOverDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.isMouseOverDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.isStylusOverDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.isKeyboardFocusWithinDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.isMouseCapturedDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.isMouseCaptureWithinDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.isStylusDirectlyOverDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.isStylusCapturedDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.isStylusCaptureWithinDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.isKeyboardFocusedDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.isInputMethodEnabledDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.opacityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.opacityMaskDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bitmapEffectDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.effectDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bitmapEffectInputDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cacheModeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.visibilityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clipToBoundsDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.clipDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.snapsToDevicePixelsDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.isFocusedDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.isEnabledDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.isHitTestVisibleDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.isVisibleDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.focusableDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.persistIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isManipulationEnabledDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.areAnyTouchesOverDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.areAnyTouchesDirectlyOverDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.areAnyTouchesCapturedWithinDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.areAnyTouchesCapturedDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.touchesCapturedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.touchesCapturedWithinDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.touchesOverDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.touchesDirectlyOverDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dependencyObjectTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isSealedDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dispatcherDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.MainWindowBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // MainWindowBindingSource
            // 
            this.MainWindowBindingSource.DataSource = typeof(WpfApp1.MainWindow);
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DataSet1";
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "ReportsApplication1.Report1.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(682, 386);
            this.reportViewer1.TabIndex = 0;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.taskbarItemInfoDataGridViewTextBoxColumn,
            this.allowsTransparencyDataGridViewCheckBoxColumn,
            this.titleDataGridViewTextBoxColumn,
            this.iconDataGridViewTextBoxColumn,
            this.sizeToContentDataGridViewTextBoxColumn,
            this.topDataGridViewTextBoxColumn,
            this.leftDataGridViewTextBoxColumn,
            this.restoreBoundsDataGridViewTextBoxColumn,
            this.windowStartupLocationDataGridViewTextBoxColumn,
            this.showInTaskbarDataGridViewCheckBoxColumn,
            this.isActiveDataGridViewCheckBoxColumn,
            this.ownerDataGridViewTextBoxColumn,
            this.ownedWindowsDataGridViewTextBoxColumn,
            this.dialogResultDataGridViewTextBoxColumn,
            this.windowStyleDataGridViewTextBoxColumn,
            this.windowStateDataGridViewTextBoxColumn,
            this.resizeModeDataGridViewTextBoxColumn,
            this.topmostDataGridViewCheckBoxColumn,
            this.showActivatedDataGridViewCheckBoxColumn,
            this.contentDataGridViewTextBoxColumn,
            this.contentTemplateDataGridViewTextBoxColumn,
            this.contentTemplateSelectorDataGridViewTextBoxColumn,
            this.contentStringFormatDataGridViewTextBoxColumn,
            this.borderBrushDataGridViewTextBoxColumn,
            this.borderThicknessDataGridViewTextBoxColumn,
            this.backgroundDataGridViewTextBoxColumn,
            this.foregroundDataGridViewTextBoxColumn,
            this.fontFamilyDataGridViewTextBoxColumn,
            this.fontSizeDataGridViewTextBoxColumn,
            this.fontStretchDataGridViewTextBoxColumn,
            this.fontStyleDataGridViewTextBoxColumn,
            this.fontWeightDataGridViewTextBoxColumn,
            this.horizontalContentAlignmentDataGridViewTextBoxColumn,
            this.verticalContentAlignmentDataGridViewTextBoxColumn,
            this.tabIndexDataGridViewTextBoxColumn,
            this.isTabStopDataGridViewCheckBoxColumn,
            this.paddingDataGridViewTextBoxColumn,
            this.templateDataGridViewTextBoxColumn,
            this.styleDataGridViewTextBoxColumn,
            this.overridesDefaultStyleDataGridViewCheckBoxColumn,
            this.useLayoutRoundingDataGridViewCheckBoxColumn,
            this.templatedParentDataGridViewTextBoxColumn,
            this.resourcesDataGridViewTextBoxColumn,
            this.dataContextDataGridViewTextBoxColumn,
            this.bindingGroupDataGridViewTextBoxColumn,
            this.languageDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn,
            this.tagDataGridViewTextBoxColumn,
            this.inputScopeDataGridViewTextBoxColumn,
            this.actualWidthDataGridViewTextBoxColumn,
            this.actualHeightDataGridViewTextBoxColumn,
            this.layoutTransformDataGridViewTextBoxColumn,
            this.widthDataGridViewTextBoxColumn,
            this.minWidthDataGridViewTextBoxColumn,
            this.maxWidthDataGridViewTextBoxColumn,
            this.heightDataGridViewTextBoxColumn,
            this.minHeightDataGridViewTextBoxColumn,
            this.maxHeightDataGridViewTextBoxColumn,
            this.flowDirectionDataGridViewTextBoxColumn,
            this.marginDataGridViewTextBoxColumn,
            this.horizontalAlignmentDataGridViewTextBoxColumn,
            this.verticalAlignmentDataGridViewTextBoxColumn,
            this.focusVisualStyleDataGridViewTextBoxColumn,
            this.cursorDataGridViewTextBoxColumn,
            this.forceCursorDataGridViewCheckBoxColumn,
            this.isInitializedDataGridViewCheckBoxColumn,
            this.isLoadedDataGridViewCheckBoxColumn,
            this.toolTipDataGridViewTextBoxColumn,
            this.contextMenuDataGridViewTextBoxColumn,
            this.parentDataGridViewTextBoxColumn,
            this.hasAnimatedPropertiesDataGridViewCheckBoxColumn,
            this.allowDropDataGridViewCheckBoxColumn,
            this.desiredSizeDataGridViewTextBoxColumn,
            this.isMeasureValidDataGridViewCheckBoxColumn,
            this.isArrangeValidDataGridViewCheckBoxColumn,
            this.renderSizeDataGridViewTextBoxColumn,
            this.renderTransformDataGridViewTextBoxColumn,
            this.renderTransformOriginDataGridViewTextBoxColumn,
            this.isMouseDirectlyOverDataGridViewCheckBoxColumn,
            this.isMouseOverDataGridViewCheckBoxColumn,
            this.isStylusOverDataGridViewCheckBoxColumn,
            this.isKeyboardFocusWithinDataGridViewCheckBoxColumn,
            this.isMouseCapturedDataGridViewCheckBoxColumn,
            this.isMouseCaptureWithinDataGridViewCheckBoxColumn,
            this.isStylusDirectlyOverDataGridViewCheckBoxColumn,
            this.isStylusCapturedDataGridViewCheckBoxColumn,
            this.isStylusCaptureWithinDataGridViewCheckBoxColumn,
            this.isKeyboardFocusedDataGridViewCheckBoxColumn,
            this.isInputMethodEnabledDataGridViewCheckBoxColumn,
            this.opacityDataGridViewTextBoxColumn,
            this.opacityMaskDataGridViewTextBoxColumn,
            this.bitmapEffectDataGridViewTextBoxColumn,
            this.effectDataGridViewTextBoxColumn,
            this.bitmapEffectInputDataGridViewTextBoxColumn,
            this.cacheModeDataGridViewTextBoxColumn,
            this.uidDataGridViewTextBoxColumn,
            this.visibilityDataGridViewTextBoxColumn,
            this.clipToBoundsDataGridViewCheckBoxColumn,
            this.clipDataGridViewTextBoxColumn,
            this.snapsToDevicePixelsDataGridViewCheckBoxColumn,
            this.isFocusedDataGridViewCheckBoxColumn,
            this.isEnabledDataGridViewCheckBoxColumn,
            this.isHitTestVisibleDataGridViewCheckBoxColumn,
            this.isVisibleDataGridViewCheckBoxColumn,
            this.focusableDataGridViewCheckBoxColumn,
            this.persistIdDataGridViewTextBoxColumn,
            this.isManipulationEnabledDataGridViewCheckBoxColumn,
            this.areAnyTouchesOverDataGridViewCheckBoxColumn,
            this.areAnyTouchesDirectlyOverDataGridViewCheckBoxColumn,
            this.areAnyTouchesCapturedWithinDataGridViewCheckBoxColumn,
            this.areAnyTouchesCapturedDataGridViewCheckBoxColumn,
            this.touchesCapturedDataGridViewTextBoxColumn,
            this.touchesCapturedWithinDataGridViewTextBoxColumn,
            this.touchesOverDataGridViewTextBoxColumn,
            this.touchesDirectlyOverDataGridViewTextBoxColumn,
            this.dependencyObjectTypeDataGridViewTextBoxColumn,
            this.isSealedDataGridViewCheckBoxColumn,
            this.dispatcherDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.MainWindowBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(682, 386);
            this.dataGridView1.TabIndex = 1;
            // 
            // taskbarItemInfoDataGridViewTextBoxColumn
            // 
            this.taskbarItemInfoDataGridViewTextBoxColumn.DataPropertyName = "TaskbarItemInfo";
            this.taskbarItemInfoDataGridViewTextBoxColumn.HeaderText = "TaskbarItemInfo";
            this.taskbarItemInfoDataGridViewTextBoxColumn.Name = "taskbarItemInfoDataGridViewTextBoxColumn";
            // 
            // allowsTransparencyDataGridViewCheckBoxColumn
            // 
            this.allowsTransparencyDataGridViewCheckBoxColumn.DataPropertyName = "AllowsTransparency";
            this.allowsTransparencyDataGridViewCheckBoxColumn.HeaderText = "AllowsTransparency";
            this.allowsTransparencyDataGridViewCheckBoxColumn.Name = "allowsTransparencyDataGridViewCheckBoxColumn";
            // 
            // titleDataGridViewTextBoxColumn
            // 
            this.titleDataGridViewTextBoxColumn.DataPropertyName = "Title";
            this.titleDataGridViewTextBoxColumn.HeaderText = "Title";
            this.titleDataGridViewTextBoxColumn.Name = "titleDataGridViewTextBoxColumn";
            // 
            // iconDataGridViewTextBoxColumn
            // 
            this.iconDataGridViewTextBoxColumn.DataPropertyName = "Icon";
            this.iconDataGridViewTextBoxColumn.HeaderText = "Icon";
            this.iconDataGridViewTextBoxColumn.Name = "iconDataGridViewTextBoxColumn";
            // 
            // sizeToContentDataGridViewTextBoxColumn
            // 
            this.sizeToContentDataGridViewTextBoxColumn.DataPropertyName = "SizeToContent";
            this.sizeToContentDataGridViewTextBoxColumn.HeaderText = "SizeToContent";
            this.sizeToContentDataGridViewTextBoxColumn.Name = "sizeToContentDataGridViewTextBoxColumn";
            // 
            // topDataGridViewTextBoxColumn
            // 
            this.topDataGridViewTextBoxColumn.DataPropertyName = "Top";
            this.topDataGridViewTextBoxColumn.HeaderText = "Top";
            this.topDataGridViewTextBoxColumn.Name = "topDataGridViewTextBoxColumn";
            // 
            // leftDataGridViewTextBoxColumn
            // 
            this.leftDataGridViewTextBoxColumn.DataPropertyName = "Left";
            this.leftDataGridViewTextBoxColumn.HeaderText = "Left";
            this.leftDataGridViewTextBoxColumn.Name = "leftDataGridViewTextBoxColumn";
            // 
            // restoreBoundsDataGridViewTextBoxColumn
            // 
            this.restoreBoundsDataGridViewTextBoxColumn.DataPropertyName = "RestoreBounds";
            this.restoreBoundsDataGridViewTextBoxColumn.HeaderText = "RestoreBounds";
            this.restoreBoundsDataGridViewTextBoxColumn.Name = "restoreBoundsDataGridViewTextBoxColumn";
            this.restoreBoundsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // windowStartupLocationDataGridViewTextBoxColumn
            // 
            this.windowStartupLocationDataGridViewTextBoxColumn.DataPropertyName = "WindowStartupLocation";
            this.windowStartupLocationDataGridViewTextBoxColumn.HeaderText = "WindowStartupLocation";
            this.windowStartupLocationDataGridViewTextBoxColumn.Name = "windowStartupLocationDataGridViewTextBoxColumn";
            // 
            // showInTaskbarDataGridViewCheckBoxColumn
            // 
            this.showInTaskbarDataGridViewCheckBoxColumn.DataPropertyName = "ShowInTaskbar";
            this.showInTaskbarDataGridViewCheckBoxColumn.HeaderText = "ShowInTaskbar";
            this.showInTaskbarDataGridViewCheckBoxColumn.Name = "showInTaskbarDataGridViewCheckBoxColumn";
            // 
            // isActiveDataGridViewCheckBoxColumn
            // 
            this.isActiveDataGridViewCheckBoxColumn.DataPropertyName = "IsActive";
            this.isActiveDataGridViewCheckBoxColumn.HeaderText = "IsActive";
            this.isActiveDataGridViewCheckBoxColumn.Name = "isActiveDataGridViewCheckBoxColumn";
            this.isActiveDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // ownerDataGridViewTextBoxColumn
            // 
            this.ownerDataGridViewTextBoxColumn.DataPropertyName = "Owner";
            this.ownerDataGridViewTextBoxColumn.HeaderText = "Owner";
            this.ownerDataGridViewTextBoxColumn.Name = "ownerDataGridViewTextBoxColumn";
            // 
            // ownedWindowsDataGridViewTextBoxColumn
            // 
            this.ownedWindowsDataGridViewTextBoxColumn.DataPropertyName = "OwnedWindows";
            this.ownedWindowsDataGridViewTextBoxColumn.HeaderText = "OwnedWindows";
            this.ownedWindowsDataGridViewTextBoxColumn.Name = "ownedWindowsDataGridViewTextBoxColumn";
            this.ownedWindowsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dialogResultDataGridViewTextBoxColumn
            // 
            this.dialogResultDataGridViewTextBoxColumn.DataPropertyName = "DialogResult";
            this.dialogResultDataGridViewTextBoxColumn.HeaderText = "DialogResult";
            this.dialogResultDataGridViewTextBoxColumn.Name = "dialogResultDataGridViewTextBoxColumn";
            // 
            // windowStyleDataGridViewTextBoxColumn
            // 
            this.windowStyleDataGridViewTextBoxColumn.DataPropertyName = "WindowStyle";
            this.windowStyleDataGridViewTextBoxColumn.HeaderText = "WindowStyle";
            this.windowStyleDataGridViewTextBoxColumn.Name = "windowStyleDataGridViewTextBoxColumn";
            // 
            // windowStateDataGridViewTextBoxColumn
            // 
            this.windowStateDataGridViewTextBoxColumn.DataPropertyName = "WindowState";
            this.windowStateDataGridViewTextBoxColumn.HeaderText = "WindowState";
            this.windowStateDataGridViewTextBoxColumn.Name = "windowStateDataGridViewTextBoxColumn";
            // 
            // resizeModeDataGridViewTextBoxColumn
            // 
            this.resizeModeDataGridViewTextBoxColumn.DataPropertyName = "ResizeMode";
            this.resizeModeDataGridViewTextBoxColumn.HeaderText = "ResizeMode";
            this.resizeModeDataGridViewTextBoxColumn.Name = "resizeModeDataGridViewTextBoxColumn";
            // 
            // topmostDataGridViewCheckBoxColumn
            // 
            this.topmostDataGridViewCheckBoxColumn.DataPropertyName = "Topmost";
            this.topmostDataGridViewCheckBoxColumn.HeaderText = "Topmost";
            this.topmostDataGridViewCheckBoxColumn.Name = "topmostDataGridViewCheckBoxColumn";
            // 
            // showActivatedDataGridViewCheckBoxColumn
            // 
            this.showActivatedDataGridViewCheckBoxColumn.DataPropertyName = "ShowActivated";
            this.showActivatedDataGridViewCheckBoxColumn.HeaderText = "ShowActivated";
            this.showActivatedDataGridViewCheckBoxColumn.Name = "showActivatedDataGridViewCheckBoxColumn";
            // 
            // contentDataGridViewTextBoxColumn
            // 
            this.contentDataGridViewTextBoxColumn.DataPropertyName = "Content";
            this.contentDataGridViewTextBoxColumn.HeaderText = "Content";
            this.contentDataGridViewTextBoxColumn.Name = "contentDataGridViewTextBoxColumn";
            // 
            // contentTemplateDataGridViewTextBoxColumn
            // 
            this.contentTemplateDataGridViewTextBoxColumn.DataPropertyName = "ContentTemplate";
            this.contentTemplateDataGridViewTextBoxColumn.HeaderText = "ContentTemplate";
            this.contentTemplateDataGridViewTextBoxColumn.Name = "contentTemplateDataGridViewTextBoxColumn";
            // 
            // contentTemplateSelectorDataGridViewTextBoxColumn
            // 
            this.contentTemplateSelectorDataGridViewTextBoxColumn.DataPropertyName = "ContentTemplateSelector";
            this.contentTemplateSelectorDataGridViewTextBoxColumn.HeaderText = "ContentTemplateSelector";
            this.contentTemplateSelectorDataGridViewTextBoxColumn.Name = "contentTemplateSelectorDataGridViewTextBoxColumn";
            // 
            // contentStringFormatDataGridViewTextBoxColumn
            // 
            this.contentStringFormatDataGridViewTextBoxColumn.DataPropertyName = "ContentStringFormat";
            this.contentStringFormatDataGridViewTextBoxColumn.HeaderText = "ContentStringFormat";
            this.contentStringFormatDataGridViewTextBoxColumn.Name = "contentStringFormatDataGridViewTextBoxColumn";
            // 
            // borderBrushDataGridViewTextBoxColumn
            // 
            this.borderBrushDataGridViewTextBoxColumn.DataPropertyName = "BorderBrush";
            this.borderBrushDataGridViewTextBoxColumn.HeaderText = "BorderBrush";
            this.borderBrushDataGridViewTextBoxColumn.Name = "borderBrushDataGridViewTextBoxColumn";
            // 
            // borderThicknessDataGridViewTextBoxColumn
            // 
            this.borderThicknessDataGridViewTextBoxColumn.DataPropertyName = "BorderThickness";
            this.borderThicknessDataGridViewTextBoxColumn.HeaderText = "BorderThickness";
            this.borderThicknessDataGridViewTextBoxColumn.Name = "borderThicknessDataGridViewTextBoxColumn";
            // 
            // backgroundDataGridViewTextBoxColumn
            // 
            this.backgroundDataGridViewTextBoxColumn.DataPropertyName = "Background";
            this.backgroundDataGridViewTextBoxColumn.HeaderText = "Background";
            this.backgroundDataGridViewTextBoxColumn.Name = "backgroundDataGridViewTextBoxColumn";
            // 
            // foregroundDataGridViewTextBoxColumn
            // 
            this.foregroundDataGridViewTextBoxColumn.DataPropertyName = "Foreground";
            this.foregroundDataGridViewTextBoxColumn.HeaderText = "Foreground";
            this.foregroundDataGridViewTextBoxColumn.Name = "foregroundDataGridViewTextBoxColumn";
            // 
            // fontFamilyDataGridViewTextBoxColumn
            // 
            this.fontFamilyDataGridViewTextBoxColumn.DataPropertyName = "FontFamily";
            this.fontFamilyDataGridViewTextBoxColumn.HeaderText = "FontFamily";
            this.fontFamilyDataGridViewTextBoxColumn.Name = "fontFamilyDataGridViewTextBoxColumn";
            // 
            // fontSizeDataGridViewTextBoxColumn
            // 
            this.fontSizeDataGridViewTextBoxColumn.DataPropertyName = "FontSize";
            this.fontSizeDataGridViewTextBoxColumn.HeaderText = "FontSize";
            this.fontSizeDataGridViewTextBoxColumn.Name = "fontSizeDataGridViewTextBoxColumn";
            // 
            // fontStretchDataGridViewTextBoxColumn
            // 
            this.fontStretchDataGridViewTextBoxColumn.DataPropertyName = "FontStretch";
            this.fontStretchDataGridViewTextBoxColumn.HeaderText = "FontStretch";
            this.fontStretchDataGridViewTextBoxColumn.Name = "fontStretchDataGridViewTextBoxColumn";
            // 
            // fontStyleDataGridViewTextBoxColumn
            // 
            this.fontStyleDataGridViewTextBoxColumn.DataPropertyName = "FontStyle";
            this.fontStyleDataGridViewTextBoxColumn.HeaderText = "FontStyle";
            this.fontStyleDataGridViewTextBoxColumn.Name = "fontStyleDataGridViewTextBoxColumn";
            // 
            // fontWeightDataGridViewTextBoxColumn
            // 
            this.fontWeightDataGridViewTextBoxColumn.DataPropertyName = "FontWeight";
            this.fontWeightDataGridViewTextBoxColumn.HeaderText = "FontWeight";
            this.fontWeightDataGridViewTextBoxColumn.Name = "fontWeightDataGridViewTextBoxColumn";
            // 
            // horizontalContentAlignmentDataGridViewTextBoxColumn
            // 
            this.horizontalContentAlignmentDataGridViewTextBoxColumn.DataPropertyName = "HorizontalContentAlignment";
            this.horizontalContentAlignmentDataGridViewTextBoxColumn.HeaderText = "HorizontalContentAlignment";
            this.horizontalContentAlignmentDataGridViewTextBoxColumn.Name = "horizontalContentAlignmentDataGridViewTextBoxColumn";
            // 
            // verticalContentAlignmentDataGridViewTextBoxColumn
            // 
            this.verticalContentAlignmentDataGridViewTextBoxColumn.DataPropertyName = "VerticalContentAlignment";
            this.verticalContentAlignmentDataGridViewTextBoxColumn.HeaderText = "VerticalContentAlignment";
            this.verticalContentAlignmentDataGridViewTextBoxColumn.Name = "verticalContentAlignmentDataGridViewTextBoxColumn";
            // 
            // tabIndexDataGridViewTextBoxColumn
            // 
            this.tabIndexDataGridViewTextBoxColumn.DataPropertyName = "TabIndex";
            this.tabIndexDataGridViewTextBoxColumn.HeaderText = "TabIndex";
            this.tabIndexDataGridViewTextBoxColumn.Name = "tabIndexDataGridViewTextBoxColumn";
            // 
            // isTabStopDataGridViewCheckBoxColumn
            // 
            this.isTabStopDataGridViewCheckBoxColumn.DataPropertyName = "IsTabStop";
            this.isTabStopDataGridViewCheckBoxColumn.HeaderText = "IsTabStop";
            this.isTabStopDataGridViewCheckBoxColumn.Name = "isTabStopDataGridViewCheckBoxColumn";
            // 
            // paddingDataGridViewTextBoxColumn
            // 
            this.paddingDataGridViewTextBoxColumn.DataPropertyName = "Padding";
            this.paddingDataGridViewTextBoxColumn.HeaderText = "Padding";
            this.paddingDataGridViewTextBoxColumn.Name = "paddingDataGridViewTextBoxColumn";
            // 
            // templateDataGridViewTextBoxColumn
            // 
            this.templateDataGridViewTextBoxColumn.DataPropertyName = "Template";
            this.templateDataGridViewTextBoxColumn.HeaderText = "Template";
            this.templateDataGridViewTextBoxColumn.Name = "templateDataGridViewTextBoxColumn";
            // 
            // styleDataGridViewTextBoxColumn
            // 
            this.styleDataGridViewTextBoxColumn.DataPropertyName = "Style";
            this.styleDataGridViewTextBoxColumn.HeaderText = "Style";
            this.styleDataGridViewTextBoxColumn.Name = "styleDataGridViewTextBoxColumn";
            // 
            // overridesDefaultStyleDataGridViewCheckBoxColumn
            // 
            this.overridesDefaultStyleDataGridViewCheckBoxColumn.DataPropertyName = "OverridesDefaultStyle";
            this.overridesDefaultStyleDataGridViewCheckBoxColumn.HeaderText = "OverridesDefaultStyle";
            this.overridesDefaultStyleDataGridViewCheckBoxColumn.Name = "overridesDefaultStyleDataGridViewCheckBoxColumn";
            // 
            // useLayoutRoundingDataGridViewCheckBoxColumn
            // 
            this.useLayoutRoundingDataGridViewCheckBoxColumn.DataPropertyName = "UseLayoutRounding";
            this.useLayoutRoundingDataGridViewCheckBoxColumn.HeaderText = "UseLayoutRounding";
            this.useLayoutRoundingDataGridViewCheckBoxColumn.Name = "useLayoutRoundingDataGridViewCheckBoxColumn";
            // 
            // templatedParentDataGridViewTextBoxColumn
            // 
            this.templatedParentDataGridViewTextBoxColumn.DataPropertyName = "TemplatedParent";
            this.templatedParentDataGridViewTextBoxColumn.HeaderText = "TemplatedParent";
            this.templatedParentDataGridViewTextBoxColumn.Name = "templatedParentDataGridViewTextBoxColumn";
            this.templatedParentDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // resourcesDataGridViewTextBoxColumn
            // 
            this.resourcesDataGridViewTextBoxColumn.DataPropertyName = "Resources";
            this.resourcesDataGridViewTextBoxColumn.HeaderText = "Resources";
            this.resourcesDataGridViewTextBoxColumn.Name = "resourcesDataGridViewTextBoxColumn";
            // 
            // dataContextDataGridViewTextBoxColumn
            // 
            this.dataContextDataGridViewTextBoxColumn.DataPropertyName = "DataContext";
            this.dataContextDataGridViewTextBoxColumn.HeaderText = "DataContext";
            this.dataContextDataGridViewTextBoxColumn.Name = "dataContextDataGridViewTextBoxColumn";
            // 
            // bindingGroupDataGridViewTextBoxColumn
            // 
            this.bindingGroupDataGridViewTextBoxColumn.DataPropertyName = "BindingGroup";
            this.bindingGroupDataGridViewTextBoxColumn.HeaderText = "BindingGroup";
            this.bindingGroupDataGridViewTextBoxColumn.Name = "bindingGroupDataGridViewTextBoxColumn";
            // 
            // languageDataGridViewTextBoxColumn
            // 
            this.languageDataGridViewTextBoxColumn.DataPropertyName = "Language";
            this.languageDataGridViewTextBoxColumn.HeaderText = "Language";
            this.languageDataGridViewTextBoxColumn.Name = "languageDataGridViewTextBoxColumn";
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            // 
            // tagDataGridViewTextBoxColumn
            // 
            this.tagDataGridViewTextBoxColumn.DataPropertyName = "Tag";
            this.tagDataGridViewTextBoxColumn.HeaderText = "Tag";
            this.tagDataGridViewTextBoxColumn.Name = "tagDataGridViewTextBoxColumn";
            // 
            // inputScopeDataGridViewTextBoxColumn
            // 
            this.inputScopeDataGridViewTextBoxColumn.DataPropertyName = "InputScope";
            this.inputScopeDataGridViewTextBoxColumn.HeaderText = "InputScope";
            this.inputScopeDataGridViewTextBoxColumn.Name = "inputScopeDataGridViewTextBoxColumn";
            // 
            // actualWidthDataGridViewTextBoxColumn
            // 
            this.actualWidthDataGridViewTextBoxColumn.DataPropertyName = "ActualWidth";
            this.actualWidthDataGridViewTextBoxColumn.HeaderText = "ActualWidth";
            this.actualWidthDataGridViewTextBoxColumn.Name = "actualWidthDataGridViewTextBoxColumn";
            this.actualWidthDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // actualHeightDataGridViewTextBoxColumn
            // 
            this.actualHeightDataGridViewTextBoxColumn.DataPropertyName = "ActualHeight";
            this.actualHeightDataGridViewTextBoxColumn.HeaderText = "ActualHeight";
            this.actualHeightDataGridViewTextBoxColumn.Name = "actualHeightDataGridViewTextBoxColumn";
            this.actualHeightDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // layoutTransformDataGridViewTextBoxColumn
            // 
            this.layoutTransformDataGridViewTextBoxColumn.DataPropertyName = "LayoutTransform";
            this.layoutTransformDataGridViewTextBoxColumn.HeaderText = "LayoutTransform";
            this.layoutTransformDataGridViewTextBoxColumn.Name = "layoutTransformDataGridViewTextBoxColumn";
            // 
            // widthDataGridViewTextBoxColumn
            // 
            this.widthDataGridViewTextBoxColumn.DataPropertyName = "Width";
            this.widthDataGridViewTextBoxColumn.HeaderText = "Width";
            this.widthDataGridViewTextBoxColumn.Name = "widthDataGridViewTextBoxColumn";
            // 
            // minWidthDataGridViewTextBoxColumn
            // 
            this.minWidthDataGridViewTextBoxColumn.DataPropertyName = "MinWidth";
            this.minWidthDataGridViewTextBoxColumn.HeaderText = "MinWidth";
            this.minWidthDataGridViewTextBoxColumn.Name = "minWidthDataGridViewTextBoxColumn";
            // 
            // maxWidthDataGridViewTextBoxColumn
            // 
            this.maxWidthDataGridViewTextBoxColumn.DataPropertyName = "MaxWidth";
            this.maxWidthDataGridViewTextBoxColumn.HeaderText = "MaxWidth";
            this.maxWidthDataGridViewTextBoxColumn.Name = "maxWidthDataGridViewTextBoxColumn";
            // 
            // heightDataGridViewTextBoxColumn
            // 
            this.heightDataGridViewTextBoxColumn.DataPropertyName = "Height";
            this.heightDataGridViewTextBoxColumn.HeaderText = "Height";
            this.heightDataGridViewTextBoxColumn.Name = "heightDataGridViewTextBoxColumn";
            // 
            // minHeightDataGridViewTextBoxColumn
            // 
            this.minHeightDataGridViewTextBoxColumn.DataPropertyName = "MinHeight";
            this.minHeightDataGridViewTextBoxColumn.HeaderText = "MinHeight";
            this.minHeightDataGridViewTextBoxColumn.Name = "minHeightDataGridViewTextBoxColumn";
            // 
            // maxHeightDataGridViewTextBoxColumn
            // 
            this.maxHeightDataGridViewTextBoxColumn.DataPropertyName = "MaxHeight";
            this.maxHeightDataGridViewTextBoxColumn.HeaderText = "MaxHeight";
            this.maxHeightDataGridViewTextBoxColumn.Name = "maxHeightDataGridViewTextBoxColumn";
            // 
            // flowDirectionDataGridViewTextBoxColumn
            // 
            this.flowDirectionDataGridViewTextBoxColumn.DataPropertyName = "FlowDirection";
            this.flowDirectionDataGridViewTextBoxColumn.HeaderText = "FlowDirection";
            this.flowDirectionDataGridViewTextBoxColumn.Name = "flowDirectionDataGridViewTextBoxColumn";
            // 
            // marginDataGridViewTextBoxColumn
            // 
            this.marginDataGridViewTextBoxColumn.DataPropertyName = "Margin";
            this.marginDataGridViewTextBoxColumn.HeaderText = "Margin";
            this.marginDataGridViewTextBoxColumn.Name = "marginDataGridViewTextBoxColumn";
            // 
            // horizontalAlignmentDataGridViewTextBoxColumn
            // 
            this.horizontalAlignmentDataGridViewTextBoxColumn.DataPropertyName = "HorizontalAlignment";
            this.horizontalAlignmentDataGridViewTextBoxColumn.HeaderText = "HorizontalAlignment";
            this.horizontalAlignmentDataGridViewTextBoxColumn.Name = "horizontalAlignmentDataGridViewTextBoxColumn";
            // 
            // verticalAlignmentDataGridViewTextBoxColumn
            // 
            this.verticalAlignmentDataGridViewTextBoxColumn.DataPropertyName = "VerticalAlignment";
            this.verticalAlignmentDataGridViewTextBoxColumn.HeaderText = "VerticalAlignment";
            this.verticalAlignmentDataGridViewTextBoxColumn.Name = "verticalAlignmentDataGridViewTextBoxColumn";
            // 
            // focusVisualStyleDataGridViewTextBoxColumn
            // 
            this.focusVisualStyleDataGridViewTextBoxColumn.DataPropertyName = "FocusVisualStyle";
            this.focusVisualStyleDataGridViewTextBoxColumn.HeaderText = "FocusVisualStyle";
            this.focusVisualStyleDataGridViewTextBoxColumn.Name = "focusVisualStyleDataGridViewTextBoxColumn";
            // 
            // cursorDataGridViewTextBoxColumn
            // 
            this.cursorDataGridViewTextBoxColumn.DataPropertyName = "Cursor";
            this.cursorDataGridViewTextBoxColumn.HeaderText = "Cursor";
            this.cursorDataGridViewTextBoxColumn.Name = "cursorDataGridViewTextBoxColumn";
            // 
            // forceCursorDataGridViewCheckBoxColumn
            // 
            this.forceCursorDataGridViewCheckBoxColumn.DataPropertyName = "ForceCursor";
            this.forceCursorDataGridViewCheckBoxColumn.HeaderText = "ForceCursor";
            this.forceCursorDataGridViewCheckBoxColumn.Name = "forceCursorDataGridViewCheckBoxColumn";
            // 
            // isInitializedDataGridViewCheckBoxColumn
            // 
            this.isInitializedDataGridViewCheckBoxColumn.DataPropertyName = "IsInitialized";
            this.isInitializedDataGridViewCheckBoxColumn.HeaderText = "IsInitialized";
            this.isInitializedDataGridViewCheckBoxColumn.Name = "isInitializedDataGridViewCheckBoxColumn";
            this.isInitializedDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // isLoadedDataGridViewCheckBoxColumn
            // 
            this.isLoadedDataGridViewCheckBoxColumn.DataPropertyName = "IsLoaded";
            this.isLoadedDataGridViewCheckBoxColumn.HeaderText = "IsLoaded";
            this.isLoadedDataGridViewCheckBoxColumn.Name = "isLoadedDataGridViewCheckBoxColumn";
            this.isLoadedDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // toolTipDataGridViewTextBoxColumn
            // 
            this.toolTipDataGridViewTextBoxColumn.DataPropertyName = "ToolTip";
            this.toolTipDataGridViewTextBoxColumn.HeaderText = "ToolTip";
            this.toolTipDataGridViewTextBoxColumn.Name = "toolTipDataGridViewTextBoxColumn";
            // 
            // contextMenuDataGridViewTextBoxColumn
            // 
            this.contextMenuDataGridViewTextBoxColumn.DataPropertyName = "ContextMenu";
            this.contextMenuDataGridViewTextBoxColumn.HeaderText = "ContextMenu";
            this.contextMenuDataGridViewTextBoxColumn.Name = "contextMenuDataGridViewTextBoxColumn";
            // 
            // parentDataGridViewTextBoxColumn
            // 
            this.parentDataGridViewTextBoxColumn.DataPropertyName = "Parent";
            this.parentDataGridViewTextBoxColumn.HeaderText = "Parent";
            this.parentDataGridViewTextBoxColumn.Name = "parentDataGridViewTextBoxColumn";
            this.parentDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // hasAnimatedPropertiesDataGridViewCheckBoxColumn
            // 
            this.hasAnimatedPropertiesDataGridViewCheckBoxColumn.DataPropertyName = "HasAnimatedProperties";
            this.hasAnimatedPropertiesDataGridViewCheckBoxColumn.HeaderText = "HasAnimatedProperties";
            this.hasAnimatedPropertiesDataGridViewCheckBoxColumn.Name = "hasAnimatedPropertiesDataGridViewCheckBoxColumn";
            this.hasAnimatedPropertiesDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // allowDropDataGridViewCheckBoxColumn
            // 
            this.allowDropDataGridViewCheckBoxColumn.DataPropertyName = "AllowDrop";
            this.allowDropDataGridViewCheckBoxColumn.HeaderText = "AllowDrop";
            this.allowDropDataGridViewCheckBoxColumn.Name = "allowDropDataGridViewCheckBoxColumn";
            // 
            // desiredSizeDataGridViewTextBoxColumn
            // 
            this.desiredSizeDataGridViewTextBoxColumn.DataPropertyName = "DesiredSize";
            this.desiredSizeDataGridViewTextBoxColumn.HeaderText = "DesiredSize";
            this.desiredSizeDataGridViewTextBoxColumn.Name = "desiredSizeDataGridViewTextBoxColumn";
            this.desiredSizeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // isMeasureValidDataGridViewCheckBoxColumn
            // 
            this.isMeasureValidDataGridViewCheckBoxColumn.DataPropertyName = "IsMeasureValid";
            this.isMeasureValidDataGridViewCheckBoxColumn.HeaderText = "IsMeasureValid";
            this.isMeasureValidDataGridViewCheckBoxColumn.Name = "isMeasureValidDataGridViewCheckBoxColumn";
            this.isMeasureValidDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // isArrangeValidDataGridViewCheckBoxColumn
            // 
            this.isArrangeValidDataGridViewCheckBoxColumn.DataPropertyName = "IsArrangeValid";
            this.isArrangeValidDataGridViewCheckBoxColumn.HeaderText = "IsArrangeValid";
            this.isArrangeValidDataGridViewCheckBoxColumn.Name = "isArrangeValidDataGridViewCheckBoxColumn";
            this.isArrangeValidDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // renderSizeDataGridViewTextBoxColumn
            // 
            this.renderSizeDataGridViewTextBoxColumn.DataPropertyName = "RenderSize";
            this.renderSizeDataGridViewTextBoxColumn.HeaderText = "RenderSize";
            this.renderSizeDataGridViewTextBoxColumn.Name = "renderSizeDataGridViewTextBoxColumn";
            // 
            // renderTransformDataGridViewTextBoxColumn
            // 
            this.renderTransformDataGridViewTextBoxColumn.DataPropertyName = "RenderTransform";
            this.renderTransformDataGridViewTextBoxColumn.HeaderText = "RenderTransform";
            this.renderTransformDataGridViewTextBoxColumn.Name = "renderTransformDataGridViewTextBoxColumn";
            // 
            // renderTransformOriginDataGridViewTextBoxColumn
            // 
            this.renderTransformOriginDataGridViewTextBoxColumn.DataPropertyName = "RenderTransformOrigin";
            this.renderTransformOriginDataGridViewTextBoxColumn.HeaderText = "RenderTransformOrigin";
            this.renderTransformOriginDataGridViewTextBoxColumn.Name = "renderTransformOriginDataGridViewTextBoxColumn";
            // 
            // isMouseDirectlyOverDataGridViewCheckBoxColumn
            // 
            this.isMouseDirectlyOverDataGridViewCheckBoxColumn.DataPropertyName = "IsMouseDirectlyOver";
            this.isMouseDirectlyOverDataGridViewCheckBoxColumn.HeaderText = "IsMouseDirectlyOver";
            this.isMouseDirectlyOverDataGridViewCheckBoxColumn.Name = "isMouseDirectlyOverDataGridViewCheckBoxColumn";
            this.isMouseDirectlyOverDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // isMouseOverDataGridViewCheckBoxColumn
            // 
            this.isMouseOverDataGridViewCheckBoxColumn.DataPropertyName = "IsMouseOver";
            this.isMouseOverDataGridViewCheckBoxColumn.HeaderText = "IsMouseOver";
            this.isMouseOverDataGridViewCheckBoxColumn.Name = "isMouseOverDataGridViewCheckBoxColumn";
            this.isMouseOverDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // isStylusOverDataGridViewCheckBoxColumn
            // 
            this.isStylusOverDataGridViewCheckBoxColumn.DataPropertyName = "IsStylusOver";
            this.isStylusOverDataGridViewCheckBoxColumn.HeaderText = "IsStylusOver";
            this.isStylusOverDataGridViewCheckBoxColumn.Name = "isStylusOverDataGridViewCheckBoxColumn";
            this.isStylusOverDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // isKeyboardFocusWithinDataGridViewCheckBoxColumn
            // 
            this.isKeyboardFocusWithinDataGridViewCheckBoxColumn.DataPropertyName = "IsKeyboardFocusWithin";
            this.isKeyboardFocusWithinDataGridViewCheckBoxColumn.HeaderText = "IsKeyboardFocusWithin";
            this.isKeyboardFocusWithinDataGridViewCheckBoxColumn.Name = "isKeyboardFocusWithinDataGridViewCheckBoxColumn";
            this.isKeyboardFocusWithinDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // isMouseCapturedDataGridViewCheckBoxColumn
            // 
            this.isMouseCapturedDataGridViewCheckBoxColumn.DataPropertyName = "IsMouseCaptured";
            this.isMouseCapturedDataGridViewCheckBoxColumn.HeaderText = "IsMouseCaptured";
            this.isMouseCapturedDataGridViewCheckBoxColumn.Name = "isMouseCapturedDataGridViewCheckBoxColumn";
            this.isMouseCapturedDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // isMouseCaptureWithinDataGridViewCheckBoxColumn
            // 
            this.isMouseCaptureWithinDataGridViewCheckBoxColumn.DataPropertyName = "IsMouseCaptureWithin";
            this.isMouseCaptureWithinDataGridViewCheckBoxColumn.HeaderText = "IsMouseCaptureWithin";
            this.isMouseCaptureWithinDataGridViewCheckBoxColumn.Name = "isMouseCaptureWithinDataGridViewCheckBoxColumn";
            this.isMouseCaptureWithinDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // isStylusDirectlyOverDataGridViewCheckBoxColumn
            // 
            this.isStylusDirectlyOverDataGridViewCheckBoxColumn.DataPropertyName = "IsStylusDirectlyOver";
            this.isStylusDirectlyOverDataGridViewCheckBoxColumn.HeaderText = "IsStylusDirectlyOver";
            this.isStylusDirectlyOverDataGridViewCheckBoxColumn.Name = "isStylusDirectlyOverDataGridViewCheckBoxColumn";
            this.isStylusDirectlyOverDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // isStylusCapturedDataGridViewCheckBoxColumn
            // 
            this.isStylusCapturedDataGridViewCheckBoxColumn.DataPropertyName = "IsStylusCaptured";
            this.isStylusCapturedDataGridViewCheckBoxColumn.HeaderText = "IsStylusCaptured";
            this.isStylusCapturedDataGridViewCheckBoxColumn.Name = "isStylusCapturedDataGridViewCheckBoxColumn";
            this.isStylusCapturedDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // isStylusCaptureWithinDataGridViewCheckBoxColumn
            // 
            this.isStylusCaptureWithinDataGridViewCheckBoxColumn.DataPropertyName = "IsStylusCaptureWithin";
            this.isStylusCaptureWithinDataGridViewCheckBoxColumn.HeaderText = "IsStylusCaptureWithin";
            this.isStylusCaptureWithinDataGridViewCheckBoxColumn.Name = "isStylusCaptureWithinDataGridViewCheckBoxColumn";
            this.isStylusCaptureWithinDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // isKeyboardFocusedDataGridViewCheckBoxColumn
            // 
            this.isKeyboardFocusedDataGridViewCheckBoxColumn.DataPropertyName = "IsKeyboardFocused";
            this.isKeyboardFocusedDataGridViewCheckBoxColumn.HeaderText = "IsKeyboardFocused";
            this.isKeyboardFocusedDataGridViewCheckBoxColumn.Name = "isKeyboardFocusedDataGridViewCheckBoxColumn";
            this.isKeyboardFocusedDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // isInputMethodEnabledDataGridViewCheckBoxColumn
            // 
            this.isInputMethodEnabledDataGridViewCheckBoxColumn.DataPropertyName = "IsInputMethodEnabled";
            this.isInputMethodEnabledDataGridViewCheckBoxColumn.HeaderText = "IsInputMethodEnabled";
            this.isInputMethodEnabledDataGridViewCheckBoxColumn.Name = "isInputMethodEnabledDataGridViewCheckBoxColumn";
            this.isInputMethodEnabledDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // opacityDataGridViewTextBoxColumn
            // 
            this.opacityDataGridViewTextBoxColumn.DataPropertyName = "Opacity";
            this.opacityDataGridViewTextBoxColumn.HeaderText = "Opacity";
            this.opacityDataGridViewTextBoxColumn.Name = "opacityDataGridViewTextBoxColumn";
            // 
            // opacityMaskDataGridViewTextBoxColumn
            // 
            this.opacityMaskDataGridViewTextBoxColumn.DataPropertyName = "OpacityMask";
            this.opacityMaskDataGridViewTextBoxColumn.HeaderText = "OpacityMask";
            this.opacityMaskDataGridViewTextBoxColumn.Name = "opacityMaskDataGridViewTextBoxColumn";
            // 
            // bitmapEffectDataGridViewTextBoxColumn
            // 
            this.bitmapEffectDataGridViewTextBoxColumn.DataPropertyName = "BitmapEffect";
            this.bitmapEffectDataGridViewTextBoxColumn.HeaderText = "BitmapEffect";
            this.bitmapEffectDataGridViewTextBoxColumn.Name = "bitmapEffectDataGridViewTextBoxColumn";
            // 
            // effectDataGridViewTextBoxColumn
            // 
            this.effectDataGridViewTextBoxColumn.DataPropertyName = "Effect";
            this.effectDataGridViewTextBoxColumn.HeaderText = "Effect";
            this.effectDataGridViewTextBoxColumn.Name = "effectDataGridViewTextBoxColumn";
            // 
            // bitmapEffectInputDataGridViewTextBoxColumn
            // 
            this.bitmapEffectInputDataGridViewTextBoxColumn.DataPropertyName = "BitmapEffectInput";
            this.bitmapEffectInputDataGridViewTextBoxColumn.HeaderText = "BitmapEffectInput";
            this.bitmapEffectInputDataGridViewTextBoxColumn.Name = "bitmapEffectInputDataGridViewTextBoxColumn";
            // 
            // cacheModeDataGridViewTextBoxColumn
            // 
            this.cacheModeDataGridViewTextBoxColumn.DataPropertyName = "CacheMode";
            this.cacheModeDataGridViewTextBoxColumn.HeaderText = "CacheMode";
            this.cacheModeDataGridViewTextBoxColumn.Name = "cacheModeDataGridViewTextBoxColumn";
            // 
            // uidDataGridViewTextBoxColumn
            // 
            this.uidDataGridViewTextBoxColumn.DataPropertyName = "Uid";
            this.uidDataGridViewTextBoxColumn.HeaderText = "Uid";
            this.uidDataGridViewTextBoxColumn.Name = "uidDataGridViewTextBoxColumn";
            // 
            // visibilityDataGridViewTextBoxColumn
            // 
            this.visibilityDataGridViewTextBoxColumn.DataPropertyName = "Visibility";
            this.visibilityDataGridViewTextBoxColumn.HeaderText = "Visibility";
            this.visibilityDataGridViewTextBoxColumn.Name = "visibilityDataGridViewTextBoxColumn";
            // 
            // clipToBoundsDataGridViewCheckBoxColumn
            // 
            this.clipToBoundsDataGridViewCheckBoxColumn.DataPropertyName = "ClipToBounds";
            this.clipToBoundsDataGridViewCheckBoxColumn.HeaderText = "ClipToBounds";
            this.clipToBoundsDataGridViewCheckBoxColumn.Name = "clipToBoundsDataGridViewCheckBoxColumn";
            // 
            // clipDataGridViewTextBoxColumn
            // 
            this.clipDataGridViewTextBoxColumn.DataPropertyName = "Clip";
            this.clipDataGridViewTextBoxColumn.HeaderText = "Clip";
            this.clipDataGridViewTextBoxColumn.Name = "clipDataGridViewTextBoxColumn";
            // 
            // snapsToDevicePixelsDataGridViewCheckBoxColumn
            // 
            this.snapsToDevicePixelsDataGridViewCheckBoxColumn.DataPropertyName = "SnapsToDevicePixels";
            this.snapsToDevicePixelsDataGridViewCheckBoxColumn.HeaderText = "SnapsToDevicePixels";
            this.snapsToDevicePixelsDataGridViewCheckBoxColumn.Name = "snapsToDevicePixelsDataGridViewCheckBoxColumn";
            // 
            // isFocusedDataGridViewCheckBoxColumn
            // 
            this.isFocusedDataGridViewCheckBoxColumn.DataPropertyName = "IsFocused";
            this.isFocusedDataGridViewCheckBoxColumn.HeaderText = "IsFocused";
            this.isFocusedDataGridViewCheckBoxColumn.Name = "isFocusedDataGridViewCheckBoxColumn";
            this.isFocusedDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // isEnabledDataGridViewCheckBoxColumn
            // 
            this.isEnabledDataGridViewCheckBoxColumn.DataPropertyName = "IsEnabled";
            this.isEnabledDataGridViewCheckBoxColumn.HeaderText = "IsEnabled";
            this.isEnabledDataGridViewCheckBoxColumn.Name = "isEnabledDataGridViewCheckBoxColumn";
            // 
            // isHitTestVisibleDataGridViewCheckBoxColumn
            // 
            this.isHitTestVisibleDataGridViewCheckBoxColumn.DataPropertyName = "IsHitTestVisible";
            this.isHitTestVisibleDataGridViewCheckBoxColumn.HeaderText = "IsHitTestVisible";
            this.isHitTestVisibleDataGridViewCheckBoxColumn.Name = "isHitTestVisibleDataGridViewCheckBoxColumn";
            // 
            // isVisibleDataGridViewCheckBoxColumn
            // 
            this.isVisibleDataGridViewCheckBoxColumn.DataPropertyName = "IsVisible";
            this.isVisibleDataGridViewCheckBoxColumn.HeaderText = "IsVisible";
            this.isVisibleDataGridViewCheckBoxColumn.Name = "isVisibleDataGridViewCheckBoxColumn";
            this.isVisibleDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // focusableDataGridViewCheckBoxColumn
            // 
            this.focusableDataGridViewCheckBoxColumn.DataPropertyName = "Focusable";
            this.focusableDataGridViewCheckBoxColumn.HeaderText = "Focusable";
            this.focusableDataGridViewCheckBoxColumn.Name = "focusableDataGridViewCheckBoxColumn";
            // 
            // persistIdDataGridViewTextBoxColumn
            // 
            this.persistIdDataGridViewTextBoxColumn.DataPropertyName = "PersistId";
            this.persistIdDataGridViewTextBoxColumn.HeaderText = "PersistId";
            this.persistIdDataGridViewTextBoxColumn.Name = "persistIdDataGridViewTextBoxColumn";
            this.persistIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // isManipulationEnabledDataGridViewCheckBoxColumn
            // 
            this.isManipulationEnabledDataGridViewCheckBoxColumn.DataPropertyName = "IsManipulationEnabled";
            this.isManipulationEnabledDataGridViewCheckBoxColumn.HeaderText = "IsManipulationEnabled";
            this.isManipulationEnabledDataGridViewCheckBoxColumn.Name = "isManipulationEnabledDataGridViewCheckBoxColumn";
            // 
            // areAnyTouchesOverDataGridViewCheckBoxColumn
            // 
            this.areAnyTouchesOverDataGridViewCheckBoxColumn.DataPropertyName = "AreAnyTouchesOver";
            this.areAnyTouchesOverDataGridViewCheckBoxColumn.HeaderText = "AreAnyTouchesOver";
            this.areAnyTouchesOverDataGridViewCheckBoxColumn.Name = "areAnyTouchesOverDataGridViewCheckBoxColumn";
            this.areAnyTouchesOverDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // areAnyTouchesDirectlyOverDataGridViewCheckBoxColumn
            // 
            this.areAnyTouchesDirectlyOverDataGridViewCheckBoxColumn.DataPropertyName = "AreAnyTouchesDirectlyOver";
            this.areAnyTouchesDirectlyOverDataGridViewCheckBoxColumn.HeaderText = "AreAnyTouchesDirectlyOver";
            this.areAnyTouchesDirectlyOverDataGridViewCheckBoxColumn.Name = "areAnyTouchesDirectlyOverDataGridViewCheckBoxColumn";
            this.areAnyTouchesDirectlyOverDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // areAnyTouchesCapturedWithinDataGridViewCheckBoxColumn
            // 
            this.areAnyTouchesCapturedWithinDataGridViewCheckBoxColumn.DataPropertyName = "AreAnyTouchesCapturedWithin";
            this.areAnyTouchesCapturedWithinDataGridViewCheckBoxColumn.HeaderText = "AreAnyTouchesCapturedWithin";
            this.areAnyTouchesCapturedWithinDataGridViewCheckBoxColumn.Name = "areAnyTouchesCapturedWithinDataGridViewCheckBoxColumn";
            this.areAnyTouchesCapturedWithinDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // areAnyTouchesCapturedDataGridViewCheckBoxColumn
            // 
            this.areAnyTouchesCapturedDataGridViewCheckBoxColumn.DataPropertyName = "AreAnyTouchesCaptured";
            this.areAnyTouchesCapturedDataGridViewCheckBoxColumn.HeaderText = "AreAnyTouchesCaptured";
            this.areAnyTouchesCapturedDataGridViewCheckBoxColumn.Name = "areAnyTouchesCapturedDataGridViewCheckBoxColumn";
            this.areAnyTouchesCapturedDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // touchesCapturedDataGridViewTextBoxColumn
            // 
            this.touchesCapturedDataGridViewTextBoxColumn.DataPropertyName = "TouchesCaptured";
            this.touchesCapturedDataGridViewTextBoxColumn.HeaderText = "TouchesCaptured";
            this.touchesCapturedDataGridViewTextBoxColumn.Name = "touchesCapturedDataGridViewTextBoxColumn";
            this.touchesCapturedDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // touchesCapturedWithinDataGridViewTextBoxColumn
            // 
            this.touchesCapturedWithinDataGridViewTextBoxColumn.DataPropertyName = "TouchesCapturedWithin";
            this.touchesCapturedWithinDataGridViewTextBoxColumn.HeaderText = "TouchesCapturedWithin";
            this.touchesCapturedWithinDataGridViewTextBoxColumn.Name = "touchesCapturedWithinDataGridViewTextBoxColumn";
            this.touchesCapturedWithinDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // touchesOverDataGridViewTextBoxColumn
            // 
            this.touchesOverDataGridViewTextBoxColumn.DataPropertyName = "TouchesOver";
            this.touchesOverDataGridViewTextBoxColumn.HeaderText = "TouchesOver";
            this.touchesOverDataGridViewTextBoxColumn.Name = "touchesOverDataGridViewTextBoxColumn";
            this.touchesOverDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // touchesDirectlyOverDataGridViewTextBoxColumn
            // 
            this.touchesDirectlyOverDataGridViewTextBoxColumn.DataPropertyName = "TouchesDirectlyOver";
            this.touchesDirectlyOverDataGridViewTextBoxColumn.HeaderText = "TouchesDirectlyOver";
            this.touchesDirectlyOverDataGridViewTextBoxColumn.Name = "touchesDirectlyOverDataGridViewTextBoxColumn";
            this.touchesDirectlyOverDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dependencyObjectTypeDataGridViewTextBoxColumn
            // 
            this.dependencyObjectTypeDataGridViewTextBoxColumn.DataPropertyName = "DependencyObjectType";
            this.dependencyObjectTypeDataGridViewTextBoxColumn.HeaderText = "DependencyObjectType";
            this.dependencyObjectTypeDataGridViewTextBoxColumn.Name = "dependencyObjectTypeDataGridViewTextBoxColumn";
            this.dependencyObjectTypeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // isSealedDataGridViewCheckBoxColumn
            // 
            this.isSealedDataGridViewCheckBoxColumn.DataPropertyName = "IsSealed";
            this.isSealedDataGridViewCheckBoxColumn.HeaderText = "IsSealed";
            this.isSealedDataGridViewCheckBoxColumn.Name = "isSealedDataGridViewCheckBoxColumn";
            this.isSealedDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // dispatcherDataGridViewTextBoxColumn
            // 
            this.dispatcherDataGridViewTextBoxColumn.DataPropertyName = "Dispatcher";
            this.dispatcherDataGridViewTextBoxColumn.HeaderText = "Dispatcher";
            this.dispatcherDataGridViewTextBoxColumn.Name = "dispatcherDataGridViewTextBoxColumn";
            this.dispatcherDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(682, 386);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.reportViewer1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.MainWindowBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource MainWindowBindingSource;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn taskbarItemInfoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn allowsTransparencyDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn titleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iconDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sizeToContentDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn topDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn leftDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn restoreBoundsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn windowStartupLocationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn showInTaskbarDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isActiveDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ownerDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ownedWindowsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dialogResultDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn windowStyleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn windowStateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn resizeModeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn topmostDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn showActivatedDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn contentDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn contentTemplateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn contentTemplateSelectorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn contentStringFormatDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn borderBrushDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn borderThicknessDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn backgroundDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn foregroundDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fontFamilyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fontSizeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fontStretchDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fontStyleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fontWeightDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn horizontalContentAlignmentDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn verticalContentAlignmentDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tabIndexDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isTabStopDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn paddingDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn templateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn styleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn overridesDefaultStyleDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn useLayoutRoundingDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn templatedParentDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn resourcesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataContextDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bindingGroupDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn languageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tagDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn inputScopeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn actualWidthDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn actualHeightDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn layoutTransformDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn widthDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn minWidthDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn maxWidthDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn heightDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn minHeightDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn maxHeightDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn flowDirectionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn marginDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn horizontalAlignmentDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn verticalAlignmentDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn focusVisualStyleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cursorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn forceCursorDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isInitializedDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isLoadedDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn toolTipDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn contextMenuDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn parentDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn hasAnimatedPropertiesDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn allowDropDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn desiredSizeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isMeasureValidDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isArrangeValidDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn renderSizeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn renderTransformDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn renderTransformOriginDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isMouseDirectlyOverDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isMouseOverDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isStylusOverDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isKeyboardFocusWithinDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isMouseCapturedDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isMouseCaptureWithinDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isStylusDirectlyOverDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isStylusCapturedDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isStylusCaptureWithinDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isKeyboardFocusedDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isInputMethodEnabledDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn opacityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn opacityMaskDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bitmapEffectDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn effectDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bitmapEffectInputDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cacheModeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn uidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn visibilityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn clipToBoundsDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn clipDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn snapsToDevicePixelsDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isFocusedDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isEnabledDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isHitTestVisibleDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isVisibleDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn focusableDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn persistIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isManipulationEnabledDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn areAnyTouchesOverDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn areAnyTouchesDirectlyOverDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn areAnyTouchesCapturedWithinDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn areAnyTouchesCapturedDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn touchesCapturedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn touchesCapturedWithinDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn touchesOverDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn touchesDirectlyOverDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dependencyObjectTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isSealedDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dispatcherDataGridViewTextBoxColumn;
    }
}

