﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nytrogen.Lib;

namespace Nytrogen.Tests
{
    [TestClass]
    public class TestFormula
    {
        [TestMethod]
        public void TestTB()
        {
            
            var alfa = 1.345;
            var gm = 0.205;
            var r = 2296;
            var result = Formula.TB(gm, alfa);
            Assert.AreEqual(r, result,"Расчет произведен неверно!");
        }

        [TestMethod]
        public void TestT()
        {
            var kp = 0.81;
            var tb = 2296;
            var r = 2005;
            var result = Formula.T(kp, tb);
            Assert.AreEqual(r, result, "Расчет произведен неверно!");
        }

        [TestMethod]
        public void TestO2I()
        {
            //arrange
            var kp = 0.81;
            var tb = 2296;
            var alfa = 1.345;
            var gm = 0.205;
            var r = 5.05;
            //act
            var result = Formula.O2I(gm, alfa,kp,tb);
            //assert
            Assert.AreEqual(r, result, "Расчет произведен неверно!");
        }

        [TestMethod]
        public void TestO2()
        {
            //arrange
            var vk = 1500;
            var vkb = 3500;
            var vbb = 47187;
            var r = 27.82;
            //act
            var result = Formula.O2(vk,vkb,vbb);
            //assert
            Assert.AreEqual(r, result, "Расчет произведен неверно!");
        }

        [TestMethod]
        public void TestVALFA()
        {
            //arrange
            var b = 133.7;
            var m = 42.85;
            var gm = 0.205;
            var qnrm = 40190;
            var qnrg = 33410;
            var alfa = 1.345;
            var o2 = 27.82;
            var r = 13.555;
            //act
            var result = Formula.VALFA(b,m,gm,qnrm,qnrg,alfa,o2);
            //assert
            Assert.AreEqual(r, result, "Расчет произведен неверно!");
        }

        [TestMethod]
        public void TestN2()
        {
            //arrange
            var o2 = 27.82;
            var alfa = 1.345;
            var gm = 0.205;
            var r = 64.27;
            //act
            var result = Formula.N2(o2, alfa, gm);
            //assert
            Assert.AreEqual(r, result, 0.1, "Расчет произведен неверно!");
        }

        [TestMethod]
        public void TestNOR()
        {
            //arrange 
            var kp = 0.81;
            var n2 = 64.27;
            var o2i = 5.05;
            var tb = 2296;
            var r = 0.00017;
            //act
            var result = Formula.NOR(kp, n2, o2i, tb);
            //assert
            Assert.AreEqual(r, result, "Расчет произведен неверно!");
        }

        [TestMethod]
        public void TestNO1()
        {
            //arrange
            var kp = 0.81;
            var nor = 0.00017;
            var tb = 2296;
            var t = 2005;
            var r = 0.00017;
            //act
            var result = Formula.NO1(kp, nor, tb, t);
            //assert
            Assert.AreEqual(r, result, "Расчет произведен неверно!");
        }

        [TestMethod]
        public void TestMCNO()
        {
            //arrange 
            var b = 133.7;
            var m = 42.85;
            var gm = 0.205;
            var qnrm = 40190;
            var nr = 0.5;
            var vkv = 0;
            var n2k = 2.85;
            var no1 = 0.00017;
            var valfa = 13.555;
            var r = 0.00019;
            //act
            var result = Formula.MCNO(b, m, gm, qnrm, nr, vkv, n2k, no1, valfa);
            //assert
            Assert.AreEqual(r, result, "Расчет произведен неверно!");
        }

        [TestMethod]
        public void TestMYNO()
        {
            //arrange 
            var b = 133.7;
            var m = 42.85;
            var gm = 0.205;
            var qnrm = 40190;
            var nr = 0.5;
            var vkv = 0;
            var n2k = 2.85;
            var no1 = 0.00017;
            var valfa = 13.555;
            var r = 0.214;
            //act
            var result = Formula.MYNO(b, m, gm, qnrm, nr, vkv, n2k, no1, valfa);
            //assert
            Assert.AreEqual(r, result, "Расчет произведен неверно!");
        }

        [TestMethod]
        public void TestMNO()
        {
            //arrange 
            var b = 133.7;
            var m = 42.85;
            var gm = 0.205;
            var qnrm = 40190;
            var nr = 0.5;
            var vkv = 0;
            var n2k = 2.85;
            var no1 = 0.00017;
            var valfa = 13.555;
            var r = 9.174;
            //act
            var result = Formula.MNO(b, m, gm, qnrm, nr, vkv, n2k, no1, valfa);
            //assert
            Assert.AreEqual(r, result, "Расчет произведен неверно!");
        }

        [TestMethod]
        public void TestVNO()
        {
            //arrange 
            var b = 133.7;
            var m = 42.85;
            var gm = 0.205;
            var qnrm = 40190;
            var nr = 0.5;
            var vkv = 0;
            var n2k = 2.85;
            var no1 = 0.00017;
            var valfa = 13.555;
            var r = 6.845;
            //act
            var result = Formula.VNO(b, m, gm, qnrm, nr, vkv, n2k, no1, valfa);
            //assert
            Assert.AreEqual(r, result, "Расчет произведен неверно!");
        }

        [TestMethod]
        public void TestNO()
        {
            //arrange
            var vno = 6.845;
            var valfa = 13.555;
            var r = 0.014;
            //act
            var result = Formula.NO(vno, valfa);
            //assert
            Assert.AreEqual(r, result, 0.00001,"Расчет произведен неверно!");
        }
    }
}
